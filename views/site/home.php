<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\model\create;
use yii\grid\GridView;
use backend\model\Posts;


/* @var $this yii\web\View */

$this->title = 'Haatch Yii CRUDE';
?>

<div class="site-index">

  <?php if (yii::$app->session->hasFlash('message')) : ?>


    <div class="alert alert-dismissible alert-success">
      <button type="button" class="close" data-dismiss="alert">&times;</button>
      <?php echo yii::$app->session->getFlash('message'); ?></div>
  <?php endif; ?>

  <div class="jumbotron">
    <h1>Haatch Yii CRUDE APPLICATION</h1>

    <!-- <p class="lead">You have successfully created your Yii-powered application.</p> -->




    <div class="row">
      <span style="margin-bottom: 20px;"><?= Html::a('Create & Get Started!', ['/site/create'], ['class' => 'btn btn-success']) ?></span>



      <div class="body-content">
        <div class="row">
          <table class="table table-condensed">

            <?php $form = ActiveForm::begin(); ?>


            <div class="row">
              <div class="row">
                <div class="form-group">
                  <div class="col-lg-2">

                    <?= $form->field($filterModel, 'cateogryId')->dropDownList($category, ['prompt' => 'Select']); ?>

                    <?= Html::submitButton('Filter', ['class' => '.btn-xs']); ?>
                    
                  </div>
                </div>



                <?php ActiveForm::end() ?>

                <thead>
                  <tr>
                    <th scope="col">ID</th>
                    <th scope="col"> Title</th>
                    <th scope="col">Description</th>
                    <th scope="col">Category</th>
                    <th scope="col">Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php if (count($posts) > 0) : ?>
                    <?php foreach ($posts as $post) : ?>
                      <tr class="table-active">
                        <th scope="row"><?php echo $post->id; ?></th>
                        <th scope="row"><?php echo $post->title; ?></th>
                        <th scope="row"><?php echo $post->description; ?></th>
                        <th scope="row"><?php echo $post->category->category; ?></th>
                        <td>
                          <span> <?= HTML::a('View', ['view', 'id' => $post->id], ['class' => 'label label-primary']) ?></span>
                          <span> <?= HTML::a('Update', ['update', 'id' => $post->id], ['class' => 'label label-default']) ?></span>
                          <span> <?= HTML::a('Delete', ['delete', 'id' => $post->id], ['class' => 'label label-danger']) ?></span>
                        </td>
                      </tr>
                    <?php endforeach; ?>
                  <?php else : ?>
                    <tr>
                      <td>No Records Found! </td>
                    </tr>
                  <?php endif; ?>


                </tbody>
          </table>


        </div>

      </div>
    </div>