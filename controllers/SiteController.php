<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Posts;
use app\models\Category;
use yii\helpers\ArrayHelper;


class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {

        //Create a dynamic model for filter method
        $model = new \yii\base\DynamicModel(['cateogryId']);
        $model->addRule(['cateogryId'], 'integer', ['max' => 11]);
        $model->addRule(['cateogryId'], 'required');
                                
        // Check if there any filter requirement, Yes -> Load the filter data and do the query.
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $posts = Posts::find()->where(['categoryid'=> $model->cateogryId])->all();
        }else{
            //Filter model is not valid. Select all data.
            $posts = Posts::find()->all();
        }
        $category = ArrayHelper::map(Category::find()->all(), 'categoryid', 'category');
        return $this->render('home', ['posts' => $posts, 'category' => $category, 'filterModel'=>$model]);
 
    }
    public function actionCreate()
    {
        $categories = ArrayHelper::map(Category::find()->all(), 'categoryid', 'category');

        $post = new Posts();
        $formData = Yii::$app->request->post();
        if ($post->load($formData)) {
            if ($post->save()) {
                Yii::$app->getSession()->setFlash('message', 'Post Creation Successfull!');
                return $this->redirect(['index']);
            } else {
                Yii::$app->getSession()->setFlash('message', 'Post Creation Failed!');
            }
        }
        return $this->render('create', ['post' => $post, 'categories' => $categories]);
    }
    public function actionView($id)
    {
        $post = Posts::find()->SELECT(['*', 'category'])->innerJoin('category', 'category.categoryid=posts.categoryid')->where(['id' => $id])->asArray()->one();

        return $this->render('view', ['post' => $post]);
    }

    public function actionUpdate($id)
    {
        // $post = Posts::find()->SELECT(['*', 'category'])->innerJoin('category', 'category.categoryid=posts.categoryid')->where(['id' => $id])->asArray()->one();
        $post = Posts::findOne($id);
        $categories = ArrayHelper::map(Category::find()->all(), 'categoryid', 'category');

        if ($post->load(Yii::$app->request->post()) && $post->save()) {
            Yii::$app->getSession()->setFlash('message', 'Post Updated Successfully!');
            return $this->redirect(['index', 'id' => $post->id]);
        } else {
            return $this->render('update', ['post' => $post, 'category' => $categories]);
        }
    }

    public function actionDelete($id)
    {
        $post = Posts::findOne($id)->delete();
        if ($post) {
            Yii::$app->getSession()->setFlash('message', 'Post Deleted Successfully!');
            return $this->redirect(['index']);
        }
    }

    public function actionGrid($id)
    {
        $post = Posts::findOne($id);
        return $this->render('homegrid');
    }



    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
